package com.eleven.pm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphPlace;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FriendPickerFragment;
import com.facebook.widget.LoginButton;
import com.facebook.widget.PickerFragment;
import com.facebook.widget.PlacePickerFragment;
//import com.facebook.samples.hellofacebook.FacebookSearch.MySimpleArrayAdapter;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity {

	private final String PENDING_ACTION_BUNDLE_KEY = "com.facebook.samples.hellofacebook:PendingAction";

	private Context appcontext;
	private LoginButton loginButton;
	private static StableArrayAdapter adapter;
	private String[] values;
	private RelativeLayout addbutton;
	private static ArrayList<String> list;
	private static final int STATIC_INTEGER_VALUE = 100;
	private static final String PUBLIC_STATIC_STRING_IDENTIFIER = "user";
	private String newText;

	private PendingAction pendingAction = PendingAction.NONE;
	private ViewGroup controlsContainer;
	private GraphUser user;
	private GraphPlace place;
	private List<GraphUser> tags;
	private boolean canPresentShareDialog;

	private enum PendingAction {
		NONE,
		POST_PHOTO,
		POST_STATUS_UPDATE
	}
	private UiLifecycleHelper uiHelper;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
			Log.d("MainActivity", String.format("Error: %s", error.toString()));
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
			Log.d("MainActivity", "Success!");
			Intent i = new Intent(appcontext, ListActivity.class);
			startActivity(i);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState0) {
		super.onCreate(savedInstanceState0);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState0);
		appcontext = getApplicationContext();

		if (savedInstanceState0 != null) {
			String name = savedInstanceState0.getString(PENDING_ACTION_BUNDLE_KEY);
			pendingAction = PendingAction.valueOf(name);
		}
		setContentView(R.layout.main);

		addbutton = (RelativeLayout) findViewById(R.id.ui_listadddelete_container); 
		addbutton.setOnTouchListener(new OnTouchListener(){

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				//makepopup();
				makealertbox();
				return false;
			}
		});

		// ArrayList<String> values = new ArrayList<String>();
		// adapter = new StableArrayAdapter(this, 10, values);

		ListView listview = (ListView) findViewById(R.id.list);

		values = new String[] { "random","names","of","random","people","are ","entered","here","like","this."};

		list = new ArrayList<String>();
		for (int i = 0; i < values.length; i++) {
			list.add(values[i]);
		};


		adapter = new StableArrayAdapter(appcontext, 
				android.R.layout.simple_list_item_1,
				//R.layout.singleelement,
				list);

		//adapter = new ArrayAdapter<String>(this, R.id.textview_addbutton, values);
		listview.setAdapter(adapter);
		listview.setVisibility(View.VISIBLE);

		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
				// final String item = (String) parent.getItemAtPosition(position);
				makealertbox(position);
			}
		});
		adapter.notifyDataSetChanged();

		loginButton = (LoginButton) findViewById(R.id.login_button);
		loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {
				MainActivity.this.user = user;
				updateUI();
				handlePendingAction();
			}
		});
		loginButton.setReadPermissions(Arrays.asList("public_profile","user_about_me"));

		controlsContainer = (ViewGroup) findViewById(R.id.main_ui_container);

		final FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragment_container);

		if (fragment != null) {
			// If we're being re-created and have a fragment, we need to a) hide the main UI controls and
			// b) hook up its listeners again.
			controlsContainer.setVisibility(View.GONE);
			if (fragment instanceof FriendPickerFragment) {
				setFriendPickerListeners((FriendPickerFragment) fragment);
			} else if (fragment instanceof PlacePickerFragment) {
				setPlacePickerListeners((PlacePickerFragment) fragment);
			}
		}

		// Listen for changes in the back stack so we know if a fragment got popped off because the user
		// clicked the back button.
		fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {
				if (fm.getBackStackEntryCount() == 0) {
					// We need to re-show our UI.
					controlsContainer.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	private void showSortPopup(final Activity context) 
	{
		// Inflate the popup_layout.xml
		LinearLayout viewGroup = (LinearLayout) context.findViewById(R.layout.activity_username_search);
		LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = layoutInflater.inflate(R.layout.activity_username_search, viewGroup);

		// Creating the PopupWindow
		PopupWindow changeSortPopUp = new PopupWindow(context);
		changeSortPopUp.setContentView(layout);
		changeSortPopUp.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
		changeSortPopUp.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
		changeSortPopUp.setFocusable(true);

		// Some offset to align the popup a bit to the left, and a bit down, relative to button's position.
		int OFFSET_X = -20;
		int OFFSET_Y = 95;

		// get screen size
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;

		// Clear the default translucent background
		// changeSortPopUp.setBackgroundDrawable(new BitmapDrawable());

		// Displaying the popup at the specified location, + offsets.
		changeSortPopUp.showAtLocation(layout, Gravity.NO_GRAVITY, width + OFFSET_X, height + OFFSET_Y);

		/*
	       // Getting a reference to Close button, and close the popup when clicked.
	       Button close = (Button) layout.findViewById(R.id.close);
	       close.setOnClickListener(new OnClickListener() {

	         @Override
	         public void onClick(View v) {
	           popup.dismiss();
	         }
	       });
		 */
	}

	/*
	public class MySimpleArrayAdapter extends ArrayAdapter<String> {
		private final Context context;
		private final ArrayList<String> values;

		public MySimpleArrayAdapter(Context context, ArrayList<String> values) {
			super(context, R.layout.listview_search, values);
			this.context = context;
			this.values = values;
		}

		@Override
		public void add(String value) {
			values.add(value);
			notifyDataSetChanged();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.listview_display, parent, false);
			TextView textView = (TextView) rowView.findViewById(R.id.name);
			textView.setText(values.get(position));
			return rowView;
		}
	};
	 */

	public void senduserdata(){
		int size = list.size();
		String[] value = new String[size];
		for (int i =0; i < size; i++){
			value[i] = list.get(i);
		};
		JSONArray arr = new JSONArray();
		if (user != null) {
			arr.put(user.getId());
			arr.put(user.getName());

			for (int i =0; i < size; i++) {
				arr.put(value[i]);
			};

			final JSONArray array0 = arr;
			Log.d("JSON ENCODE::", arr.toString());

			Runnable runnable = new Runnable() {
				public void run() {     	

					// Create a new HttpClient and Post Header
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(getResources().getString(R.string.post_array_url));
					HttpContext httpContext = new BasicHttpContext();

					try {

						// Add your data
						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("list_array", array0.toString()));
						httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
						httppost.setHeader("Accept", "application/json");
						httppost.setHeader("Content-type", "application/json");

						/*
						StringEntity se = new StringEntity(array0.toString());
						httppost.setEntity(se);
						httppost.setHeader("Accept", "application/json");
						httppost.setHeader("Content-type", "application/json");
						 */

						// Execute HTTP Post Request
						HttpResponse response = httpclient.execute(httppost, httpContext);

						//execute your request and parse response
						// HttpEntity entity = response.getEntity();
						Log.d("status code", "" + response.getStatusLine().getStatusCode());

					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			};

			Thread mythread = new Thread(runnable);
			mythread.start();

		} else {
			Toast.makeText(appcontext, "Login to facebook.", Toast.LENGTH_SHORT).show();
		}
	};

	private void makepopup(){
		Intent intent = new Intent(appcontext, UsernameSearch.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		// startActivityForResult(intent, res);
		startActivityForResult(intent, STATIC_INTEGER_VALUE);
	}

	private void makealertbox(){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		alert.setTitle("Add username");
		alert.setMessage("Enter username in box below");

		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setSingleLine();
		alert.setView(input);

		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// newText = input.getText().toString();
				MainActivity.adddata(input.getText().toString());
			}
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.

			}
		});
		alert.show();
	}

	static public void adddata(String value){

		list.add(value);
		adapter.notifyDataSetChanged();
	}


	private void makealertbox(final int position) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Delete user");
		builder.setMessage("Are you sure you want to delete user?");
		builder.setPositiveButton("Yes", new android.content.DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO if (user != null) {
				if (!(list.size() < position)){
					list.remove(position-list.size());
					adapter.notifyDataSetChanged();
					adapter.notifyDataSetInvalidated();
					senduserdata();
				} else {
					Toast.makeText(appcontext, "position:"+position, Toast.LENGTH_SHORT).show();
				}
			}

		});
		builder.setNegativeButton("No", new android.content.DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
			}

		});

		AlertDialog imageDialogAlert = builder.create();
		imageDialogAlert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		imageDialogAlert.show();
	}


	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
		/*
		Intent intent = getIntent();
		if (intent != null) {
			list = new ArrayList<String>();
			for (int i = 0; i < values.length; i++) {
				list.add(values[i]);
			};
			list.add(newText);
			adapter.notifyDataSetChanged();
			senduserdata();
		}
		 */
		AppEventsLogger.activateApp(this);
		updateUI();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
		outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) { 
		case (STATIC_INTEGER_VALUE) : { 
			if (resultCode == Activity.RESULT_OK) { 
				newText = data.getStringExtra(PUBLIC_STATIC_STRING_IDENTIFIER);
				Toast.makeText(appcontext, newText, Toast.LENGTH_SHORT).show();
				list.add(newText);
				adapter.notifyDataSetChanged();
			}
			break; 
		}
		default: {
			uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
		}
		} 
	}


	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();

		// Call the 'deactivateApp' method to log an app event for use in analytics and advertising
		// reporting.  Do so in the onPause methods of the primary Activities that an app may be launched into.
		AppEventsLogger.deactivateApp(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	private void onSessionStateChange(Session session, SessionState state, Exception exception) {

		if (pendingAction != PendingAction.NONE &&
				(exception instanceof FacebookOperationCanceledException ||
						exception instanceof FacebookAuthorizationException)) {
			new AlertDialog.Builder(MainActivity.this)
			.setTitle(R.string.cancelled)
			.setMessage(R.string.permission_not_granted)
			.setPositiveButton(R.string.ok, null)
			.show();
			pendingAction = PendingAction.NONE;
		} else if (state == SessionState.OPENED_TOKEN_UPDATED) {
			handlePendingAction();
		}
		updateUI();
	}

	private String buildUserInfoDisplay(GraphUser user) {
		StringBuilder userInfo = new StringBuilder("");

		// Example: typed access (name)
		// - no special permissions required
		userInfo.append(String.format("Name: %s\n\n", 
				user.getName()));

		// Example: typed access (birthday)
		// - requires user_birthday permission
		userInfo.append(String.format("Birthday: %s\n\n", 
				user.getBirthday()));

		// Example: partially typed access, to location field,
		// name key (location)
		// - requires user_location permission
		userInfo.append(String.format("Location: %s\n\n", 
				user.getLocation().getProperty("name")));

		// Example: access via property name (locale)
		// - no special permissions required
		userInfo.append(String.format("Locale: %s\n\n", 
				user.getProperty("locale")));

		//	    // Example: access via key for array (languages) 
		//	    // - requires user_likes permission
		//	    JSONArray languages = (JSONArray)user.getProperty("languages");
		//	    if (languages.length() > 0) {
		//	        ArrayList<String> languageNames = new ArrayList<String> ();
		//	        for (int i=0; i < languages.length(); i++) {
		//	            JSONObject language = languages.optJSONObject(i);
		//	            // Add the language name to a list. Use JSON
		//	            // methods to get access to the name field. 
		//	            languageNames.add(language.optString("name"));
		//	        }           
		//	        userInfo.append(String.format("Languages: %s\n\n", 
		//	        languageNames.toString()));
		//	    }

		return userInfo.toString();
	}

	private void updateUI() {
		Session session = Session.getActiveSession();
	}

	@SuppressWarnings("incomplete-switch")
	private void handlePendingAction() {
		PendingAction previouslyPendingAction = pendingAction;
		// These actions may re-set pendingAction if they are still pending, but we assume they
		// will succeed.
		pendingAction = PendingAction.NONE;

		switch (previouslyPendingAction) {
		case POST_PHOTO:
			break;
		case POST_STATUS_UPDATE:
			postStatusUpdate();
			break;
		}
	}

	private interface GraphObjectWithId extends GraphObject {
		String getId();
	}

	private void showPublishResult(String message, GraphObject result, FacebookRequestError error) {
		String title = null;
		String alertMessage = null;
		if (error == null) {
			title = getString(R.string.success);
			String id = result.cast(GraphObjectWithId.class).getId();
			alertMessage = getString(R.string.successfully_posted_post, message, id);
		} else {
			title = getString(R.string.error);
			alertMessage = error.getErrorMessage();
		}

		new AlertDialog.Builder(this)
		.setTitle(title)
		.setMessage(alertMessage)
		.setPositiveButton(R.string.ok, null)
		.show();
	}

	private FacebookDialog.ShareDialogBuilder createShareDialogBuilderForLink() {
		return new FacebookDialog.ShareDialogBuilder(this)
		.setName("11pm")
		.setDescription("")
		.setLink("");
	}

	private void postStatusUpdate() {
		if (canPresentShareDialog) {
			FacebookDialog shareDialog = createShareDialogBuilderForLink().build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		} else if (user != null && hasPublishPermission()) {
			final String message = getString(R.string.status_update, user.getFirstName(), (new Date().toString()));
			Request request = Request
					.newStatusUpdateRequest(Session.getActiveSession(), message, place, tags, new Request.Callback() {
						@Override
						public void onCompleted(Response response) {
							showPublishResult(message, response.getGraphObject(), response.getError());
						}
					});
			request.executeAsync();
		} else {
			pendingAction = PendingAction.POST_STATUS_UPDATE;
		}
	}


	private void setFriendPickerListeners(final FriendPickerFragment fragment) {
		fragment.setOnDoneButtonClickedListener(new FriendPickerFragment.OnDoneButtonClickedListener() {
			@Override
			public void onDoneButtonClicked(PickerFragment<?> pickerFragment) {
				onFriendPickerDone(fragment);
			}
		});
	}

	private void onFriendPickerDone(FriendPickerFragment fragment) {
		FragmentManager fm = getSupportFragmentManager();
		fm.popBackStack();

		String results = "";

		List<GraphUser> selection = fragment.getSelection();
		tags = selection;
		if (selection != null && selection.size() > 0) {
			ArrayList<String> names = new ArrayList<String>();
			for (GraphUser user : selection) {
				names.add(user.getName());
			}
			results = TextUtils.join(", ", names);
		} else {
			results = getString(R.string.no_friends_selected);
		}

		showAlert(getString(R.string.you_picked), results);
	}

	private void onPlacePickerDone(PlacePickerFragment fragment) {
		FragmentManager fm = getSupportFragmentManager();
		fm.popBackStack();

		String result = "";

		GraphPlace selection = fragment.getSelection();
		if (selection != null) {
			result = selection.getName();
		} else {
			result = getString(R.string.no_place_selected);
		}

		place = selection;

		showAlert(getString(R.string.you_picked), result);
	}

	private void setPlacePickerListeners(final PlacePickerFragment fragment) {
		fragment.setOnDoneButtonClickedListener(new PlacePickerFragment.OnDoneButtonClickedListener() {
			@Override
			public void onDoneButtonClicked(PickerFragment<?> pickerFragment) {
				onPlacePickerDone(fragment);
			}
		});
		fragment.setOnSelectionChangedListener(new PlacePickerFragment.OnSelectionChangedListener() {
			@Override
			public void onSelectionChanged(PickerFragment<?> pickerFragment) {
				if (fragment.getSelection() != null) {
					onPlacePickerDone(fragment);
				}
			}
		});
	}

	private void showAlert(String title, String message) {
		new AlertDialog.Builder(this)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(R.string.ok, null)
		.show();
	}

	private boolean hasPublishPermission() {
		Session session = Session.getActiveSession();
		return session != null && session.getPermissions().contains("publish_actions");
	}

	@SuppressLint("ValidFragment")
	public class MyDialogFragment extends DialogFragment{
		Context mContext;
		public MyDialogFragment() {
			mContext = getActivity();
		}
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(appcontext);
			alertDialogBuilder.setTitle("Really?");
			alertDialogBuilder.setMessage("Are you sure?");
			//null should be your on click listener
			alertDialogBuilder.setPositiveButton("OK", null);
			alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});


			return alertDialogBuilder.create();
		}
	}

	private class StableArrayAdapter extends ArrayAdapter<String> {

		public StableArrayAdapter(Context context, int resource,
				List<String> objects) {
			super(context, resource, objects);
		}
	}
}