package com.eleven.pm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class UsernameSearch extends Activity {
	protected static final String PUBLIC_STATIC_STRING_IDENTIFIER = "user";
	Context appcontext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_username_search);
		appcontext = getApplicationContext();
		final EditText search = (EditText) findViewById(R.id.edittext_searchbox);
		search.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					// hide virtual keyboard
					InputMethodManager imm = (InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
					String usertoadd = search.getText().toString();
					Intent resultIntent = new Intent();
					resultIntent.putExtra(PUBLIC_STATIC_STRING_IDENTIFIER, usertoadd);
					setResult(Activity.RESULT_OK, resultIntent);
					finish();
					
					/*if (usertoadd.compareTo("") != 0 ){
						Intent i = new Intent(appcontext, MainActivity.class);
						i.putExtra("user", usertoadd);
						startActivity(i);
					};
					*/
					
					return false;
				}
				return false;
			}
		});

	}
}
