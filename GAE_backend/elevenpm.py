#!/usr/bin/env python

import cgi
import datetime
import webapp2
import json

from google.appengine.ext import ndb
from google.appengine.api import users


class User(ndb.Model):
  author = ndb.UserProperty()
  username = ndb.TextProperty()
  array_list = ndb.TextProperty()
  userid = ndb.FloatProperty()
  date = ndb.DateTimeProperty(auto_now_add=True)


class MainPage(webapp2.RequestHandler):
	def get(self):
		self.response.out.write("Ok")

	def post(self):
		array = self.request.get("data")
		data = json.loads(array)
		list =''
		for element in data[2:]:
			list=element+','+list
			
		flag = False
		for user in User.query():
			if (user.userid == data[0]): 
				user.array_list = list
				user.put()
				flag = True
				self.response.out.write("received")
			
		if (flag == False):
			user = User(username=data[1], userid=data[0], array_list=list)
			User.put(user)
			self.response.out.write("received")

app = webapp2.WSGIApplication([
  ('/', MainPage)
], debug=True)
